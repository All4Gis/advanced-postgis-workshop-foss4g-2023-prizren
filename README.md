sAdvanced Postgis Workshop FOSS4G 2023 Prizren
==============================================

Requirements
------------

 - Git.
 - Containers. Docker/Podman. If able to orchestrate a docker-compose.yml file then you are on the right track.
 - A network connection.

