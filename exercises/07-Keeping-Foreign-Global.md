Keeping Foreign Global: Travelling Vicariously Without Moving
=============================================================

## The Scenario: How Can we Get There?

Your friends are very excited that you are going to FOSS4G 2023 in Prizren, Kosovo. They now want to know which countries they would travel over if they went to Prizren from their home town. But they aren't in posession of any desktop tools or postgis (They just aren't techie like that, despite your best efforts to encourage them). So, you decide to create a website for them. They can click on a map, and the path to Prizren along with the countries they fly over will be displayed.

```SQL
/*
This is the weirdest most obscure bug ever.
Some strange interaction between ogr_fdw and PostgREST
*/
DROP SERVER natural_earth CASCADE ;
```

## The Tools

So, to start, you did the bulk of the work in Exercise 2. What is now needed is:
 1. a way to get data from the database out to the web, and
 2. an easily repeatable way of encapsulating the process you did in Exercise 2 with new coordinates.

Lets discuss #1. The web. HTML/CSS/JS. That is basicaly it. And usually some form of data transfer from any servers to the front-end. It used to XML or SOAP but it is overwhelmingly JSON. All of this communication takes place by way of the HTTP(s) protocol. But Postgresql doesn't speak HTTP(s) natively. You need some sort of translation layer between PostreSQL and the frontend. Historically, you would write some code in your favourite language, usually an ORM would be involved to hide the details of SQL from you. But, firstly, why? Why hide the SQL, it is such a usefull language already, why stuff it behind an ORM, that in some cases, doesn't understand the intricacies of the interaction. In the past 5 years or so,

Lets start by trying to solve #2 first. Generally if you want to make something easily repeatable you would use a function. Postgresql has several procedural languages which can be used to create functions. We're gonna stick to plpgsql.

We create a schema to store our web stuff:



```SQL
CREATE SCHEMA api;
```
Open `http://localhost:44440/keeping-foreign-global.html` in a browser and then open developer tools.

Lets start simply by trying to get the path. The function takes in the lat & lon from the point that was clicked in the browser window and returns geojson.

```SQL
CREATE OR REPLACE FUNCTION api.to_prizren(IN lng float, IN lat float, OUT info json)
AS
$$
 BEGIN
  -- We've seen both the st_makeline & st_makepoint functions.
  -- st_asgeojson does what you expect, converts a geometry/geography into a geoJSON fragment.
  info = '{"type": "Feature","properties": {"made-by":"rhys"}, "geometry":' ||st_asgeojson(st_makeline(st_makepoint(lng, lat),st_makepoint(22.22, 42.42)))||'}';
 END;
$$
LANGUAGE plpgsql;
NOTIFY pgrst, 'reload schema';
```


A couple things to note, we do not need an explicit `RETURN` as we are using IN & OUT attributes.
So, we have the path working, lets try and get the countries as well.

```SQL
CREATE OR REPLACE FUNCTION api.to_prizren(IN lng float, IN lat float, OUT info json)
AS
$$
 DECLARE
  path geometry(Linestring, 4326);
  pays geometry;

 BEGIN
  path = st_makeline(st_makepoint(lng, lat),st_makepoint(22.22, 42.42));

  SELECT st_union(geo::geometry) FROM natural_earth.countries where
  st_intersects(path, geo) INTO pays;

  info = st_asgeojson(st_collect(path, pays));

END;
$$
LANGUAGE plpgsql;
NOTIFY pgrst, 'reload schema';
```

Hoooray!!!!!! We're making progress!!!!

Depending on where you clicked, you may notice that the path does not intersect one or more countries. This can be easily fixed using st_segmentize.

```SQL
CREATE OR REPLACE FUNCTION api.to_prizren(IN lng float, IN lat float, OUT info json)
AS
$$
 DECLARE
  path geography(Linestring, 4326);
  pays geography;

 BEGIN
  path = st_segmentize(st_makeline(st_makepoint(lng, lat),st_makepoint(22.22, 42.42))::geography, 50000);

  SELECT st_union(geo::geometry) FROM natural_earth.countries where
  st_intersects(path, geo) INTO pays;

  info = st_asgeojson(st_collect(path::geometry, pays::geometry));

END;
$$
LANGUAGE plpgsql;
NOTIFY pgrst, 'reload schema';
```

Ok, your friends now have a nifty website to see which countries they will fly over on their way to Firenze. It just needs a bit of love in the style department.

