Victorious Vectors: Tiling like you mean it
===========================================

## The Scenario: Its Tile Time!!!




CREATE OR REPLACE FUNCTION api.tiles(
	x integer,
	y integer,
	z integer
)
    RETURNS bytea
    IMMUTABLE STRICT PARALLEL SAFE
AS $BODY$
DECLARE
  bounds geometry;
  mvt bytea;
BEGIN
  PERFORM set_config('response.headers', '[{"Content-Type": "application/octet-stream"}, {"Access-Control-Allow-Headers": "*"} ]', true);

	bounds := st_tileenvelope(z, x, y);
-- 	mvt :=  st_asmvtgeom(bounds, bounds);
	/*
	SELECT st_asmvt(foo.*, 'hipity') FROM (
			SELECT 'center' which_part, st_asmvtgeom(st_centroid(bounds),bounds)
			UNION ALL
			SELECT 'boundary', st_asmvtgeom(bounds, bounds)
		) as foo into mvt;
	*/
SELECT (
	SELECT st_asmvt(foo.*, 'hipity') FROM (
			SELECT 'center' which_part, st_asmvtgeom(st_centroid(bounds),bounds)
			) as foo) ||
	(SELECT st_asmvt(foo.*, 'hopity') FROM (
			SELECT 'boundary' which_part, st_asmvtgeom(bounds, bounds)
		) as foo) into mvt;
	return mvt;
end;
$BODY$
language plpgsql;
